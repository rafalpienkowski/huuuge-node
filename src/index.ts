import express, { RequestHandler, Request, Response, NextFunction } from 'express'
import routes from './routes';
import { paymentsRoutes } from './routes/payments';

const app = express()

app.use(routes)
app.use('/payments', paymentsRoutes)

app.listen(8080);

