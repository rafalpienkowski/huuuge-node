import { Router } from "express";
import { usersRoutes } from "./users";

const routes = Router()

  .use('/users', usersRoutes)


export default routes;